package com.example.samplerestrung;

import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

import com.example.samplerestrung.lib.rest.cache.RequestCache;
import com.example.samplerestrung.lib.rest.client.ContextAwareAPIDelegate;
import com.example.samplerestrung.lib.rest.client.RestClientFactory;

public class MainActivity extends BaseActivity implements OnClickListener {

	private static final String SERVER_IP = "176.58.98.46";

	private Button getMethodButton;
	private Button cacheMethodButton;
	private Button postMethodButton;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		initViewControls();
	}

	private void initViewControls() {
		getMethodButton = (Button) findViewById(R.id.activity_main_get_method);
		cacheMethodButton = (Button) findViewById(R.id.activity_main_cache_get_method);
		postMethodButton = (Button) findViewById(R.id.activity_main_post_method);

		postMethodButton.setOnClickListener(this);
		getMethodButton.setOnClickListener(this);
		cacheMethodButton.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		// case R.id.activity_main_get_method:
		// getMethod();
		// break;
		//
		// case R.id.activity_main_cache_get_method:
		// cacheMethod();
		// break;
		//
		// case R.id.activity_main_post_method:
		// postMethod();
		// break;
		default:
			break;
		}

	}

	private void postMethod() {
		// An object that implements JSONSerializable
		AnyBeanObject sourceObject = new AnyBeanObject();
		sourceObject.setUsername("admin#FE");
		sourceObject.setPassword("21232f297a57a5a743894a0e4a801fc3");

		RestClientFactory.getClient().postAsync(
				new ContextAwareAPIDelegate<Response>(this, Response.class) {
					@Override
					public void onResults(Response result) {
						if (result != null) {
							System.out.println("Post Call Response :: "
									+ result.toString());
						}
					}

					@Override
					public void onError(Throwable e) {
						// TODO Auto-generated method stub
					}

				}, "http://" + SERVER_IP + ":8080/api/login", sourceObject,
				RequestCache.LoadPolicy.ENABLED);

	}

	private void cacheMethod() {

		RestClientFactory.getClient().getAsync(
				new ContextAwareAPIDelegate<Response>(this, Response.class,
						RequestCache.LoadPolicy.NETWORK_ENABLED) {
					@Override
					public void onResults(Response result) {
						if (result != null) {
							System.out.println(" Cache Result :: "
									+ result.toString());
						}
					}

					@Override
					public void onError(Throwable e) {
						// TODO Auto-generated method stub

					}

				}, "http://" + SERVER_IP + ":8080/api/content/HELP", "");

	}

	private void getMethod() {

		RestClientFactory
				.getClient()
				.getAsync(
						new ContextAwareAPIDelegate<Response>(this,
								Response.class) {

							@Override
							public void onResults(Response response) {
								if (response != null) {
									System.out.println(" RESPONSE :: "
											+ response.toString());
								}
							}

							@Override
							public void onError(Throwable e) {
								// handle errors here in the main thread
							}

							// /api/getRestaurantDetail?id=1&lat=25.8797&lng=72.97979

							// http://" + SERVER_IP + ":8080/api/content/HELP

						},
						"http://"
								+ SERVER_IP
								+ ":8080/api/getRestaurantDetail?id=1&long=77.521425&lat=28.451263",
						"");

	}

}
