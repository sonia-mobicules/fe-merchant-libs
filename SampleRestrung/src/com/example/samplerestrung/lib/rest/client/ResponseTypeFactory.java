package com.example.samplerestrung.lib.rest.client;

import com.example.samplerestrung.lib.rest.marshalling.response.JSONResponse;


public interface ResponseTypeFactory {

    <T extends JSONResponse> T newInstance(Class<T> targetClass);

}
