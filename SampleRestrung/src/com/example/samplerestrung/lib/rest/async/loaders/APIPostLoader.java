/*
 * Copyright (C) 2012 47 Degrees, LLC
 * http://47deg.com
 * hello@47deg.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package com.example.samplerestrung.lib.rest.async.loaders;

import java.io.File;
import java.util.concurrent.Callable;

import com.example.samplerestrung.lib.rest.async.AsyncOperation;
import com.example.samplerestrung.lib.rest.client.APICredentialsDelegate;
import com.example.samplerestrung.lib.rest.client.APIDelegate;
import com.example.samplerestrung.lib.rest.client.APIPostParams;
import com.example.samplerestrung.lib.rest.client.RestClientFactory;
import com.example.samplerestrung.lib.rest.marshalling.request.JSONSerializable;
import com.example.samplerestrung.lib.rest.marshalling.response.JSONResponse;

public class APIPostLoader<T extends JSONResponse> extends APILoader<T> {

    /**
     * @param delegate               the delegate that will be notified on successful requests
     * @param apiCredentialsDelegate the credentials delegate
     * @param url                    the service endpoint
     * @param body                   the json body to be sent as post
     * @param file                   an optional file
     * @param params                 the params to be replaced on the url placeholders
     */
    public APIPostLoader(APIDelegate<T> delegate, APICredentialsDelegate apiCredentialsDelegate, APIPostParams delegateParams, String url, JSONSerializable body, File file, Object... params) {
        super(delegate, apiCredentialsDelegate, url, body, file, delegateParams, params);
    }

    @Override
    public Callable<T> getCallable() {
        return new Callable<T>() {
            @Override
            public T call() throws Exception {
                return getOperation().executeWithExceptionHandling(new Callable<T>() {
                    @Override
                    public T call() throws Exception {
                        return RestClientFactory.getClient().post(getOperation().getApiDelegate(), getOperation().getUrl(), getOperation().getBody(), getOperation().getFile(), AsyncOperation.DEFAULT_REQUEST_TIMEOUT, getOperation().getPostDelegateParams());
                    }
                });
            }
        };
    }
}
