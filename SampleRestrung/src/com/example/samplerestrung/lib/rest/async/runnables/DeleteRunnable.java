package com.example.samplerestrung.lib.rest.async.runnables;

import com.example.samplerestrung.lib.rest.async.asynctasks.APIDeleteAsyncTask;
import com.example.samplerestrung.lib.rest.async.loaders.APIDeleteLoader;
import com.example.samplerestrung.lib.rest.client.APIDelegate;
import com.example.samplerestrung.lib.rest.marshalling.request.JSONSerializable;
import com.example.samplerestrung.lib.rest.marshalling.response.JSONResponse;

/**
 * Runnable operation for DELETE requests
 *
 * @param <T> the response type
 */
public class DeleteRunnable<T extends JSONResponse> extends AbstractCacheAwareRunnable<T> {

    /**
     * An object to be serialized and sent in the request body
     */
    private JSONSerializable body;

    /**
     * Constructs a GET runnable operation
     *
     * @see AbstractCacheAwareRunnable#AbstractCacheAwareRunnable(it.restrung.rest.client.APIDelegate, String, Object[])
     */
    public DeleteRunnable(APIDelegate<T> delegate, String url, JSONSerializable body, Object[] args) {
        super(delegate, url, args);
        this.body = body;
    }

    /**
     * @see AbstractCacheAwareRunnable#executeAsyncTask()
     */
    @Override
    public void executeAsyncTask() {
        new APIDeleteAsyncTask<T>(getUrl(), getDelegate(), null, body, getArgs()).execute();
    }

    /**
     * @see AbstractCacheAwareRunnable#executeLoader()
     */
    @Override
    public void executeLoader() {
        new APIDeleteLoader<T>(getUrl(), getDelegate(), null, body, getArgs()).execute();
    }
}
