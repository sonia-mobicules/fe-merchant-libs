package com.example.samplerestrung;

import com.example.samplerestrung.lib.rest.marshalling.request.JSONSerializable;

public class AnyBeanObject implements JSONSerializable {

	private String username;
	private String password;

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Override
	public String toJSON() {
		String requestBody = "username=" + username + "&" + "password="
				+ password;

		return requestBody;

	}

}
